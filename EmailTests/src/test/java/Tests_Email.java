import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Tests_Email {

   @Before
    public void before() {
        WebDriverManager.chromedriver().setup();
    }

    @After
    public void after() {
       MyWebDriver.getDriver().quit();
    }

    @Test
    public void login(){
       LoginPage loginPage = new LoginPage();
       loginPage.open();
       loginPage.getLoginField().click();
       loginPage.getLoginField().sendKeys("tester223@meta.ua");
       loginPage.getPasswordField().click();
       loginPage.getPasswordField().sendKeys("porosia");
       loginPage.getSignInButton().click();
       new WebDriverWait(MyWebDriver.getDriver(), 10).until(ExpectedConditions.visibilityOf(new MainPage().getMakeLetter()));
       MainPage mainPage = new MainPage();
       int numberOfInboxLettersInitial = Integer.parseInt(mainPage.getNumberOfInbox().getText().split("/")[1].trim());
        System.out.println("Initial number of letters:" + numberOfInboxLettersInitial);
       mainPage.getMakeLetter().click();
        new WebDriverWait(MyWebDriver.getDriver(), 10).until(ExpectedConditions.visibilityOf(new MakeLetterPage().getSendButton()));

        MakeLetterPage makeLetterPage = new MakeLetterPage();
        makeLetterPage.getSendToField().click();
        makeLetterPage.getSendToField().sendKeys("tester223@meta.ua");
        makeLetterPage.getSubjectField().click();
        makeLetterPage.getSubjectField().sendKeys("This is a test");
        makeLetterPage.getInputLetterField().click();
        makeLetterPage.getInputLetterField().sendKeys("This is the body of test letter");
        makeLetterPage.getSendButton().click();
        new WebDriverWait(MyWebDriver.getDriver(), 10).until(ExpectedConditions.visibilityOf(new MainPage().getModalLetterIsSent()));
        mainPage.getCloseModalWindowOK().click();
        int numberOfInboxLettersUpdated = Integer.parseInt(mainPage.getNumberOfInbox().getText().split("/")[1].trim());
        System.out.println("Initial number of letters:" + numberOfInboxLettersUpdated);
        Assert.assertEquals("Numbers of letters are different", numberOfInboxLettersInitial + 1, numberOfInboxLettersUpdated);








    }
}
