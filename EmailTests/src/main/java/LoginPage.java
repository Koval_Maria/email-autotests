import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
@Setter
@EqualsAndHashCode

public class LoginPage {
    LoginPage() {
        PageFactory.initElements(MyWebDriver.getDriver(), this);
    }

    @FindBy(css = "[id ='loginForm'] [id='login-field']")
    private WebElement loginField;
    @FindBy(css = "[id='loginForm'] [id='pass-field']")
    private WebElement passwordField;
    @FindBy(css = "[id='loginForm'] [id='loginbtnua']")
    private WebElement signInButton;


    public void open() {
        MyWebDriver.getDriver().get("https://webmail.meta.ua/");

    }

    public WebElement getLoginField() {
        return loginField;
    }

    public void setLoginField(WebElement loginField) {
        this.loginField = loginField;
    }

    public WebElement getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(WebElement passwordField) {
        this.passwordField = passwordField;
    }

    public WebElement getSignInButton() {
        return signInButton;
    }

    public void setSignInButton(WebElement signInButton) {
        this.signInButton = signInButton;
    }
}
