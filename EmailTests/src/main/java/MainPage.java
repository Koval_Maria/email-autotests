import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    MainPage() {
        PageFactory.initElements(MyWebDriver.getDriver(), this);
    }

    @FindBy(css = "[class='left_panel_block'] a[id='id_send_email']")
    private WebElement makeLetter;
    @FindBy(css = "[class='left_panel_block'] [class='treeContainer1'] [class='treeElement grayFon']")
    private WebElement numberOfInbox;
    @FindBy(css = "[id='b3_c'] [id='allcheck']")
    private WebElement allCheckbox;
    @FindBy(css = "[id='b3_c'] [id='id_delete']")
    private WebElement deleteButton;
    @FindBy(css = "[class='send_ok']")
    private WebElement modalLetterIsSent;
    @FindBy(css = "[id='close_send_ok']")
    private WebElement closeModalWindowOK;

    public WebElement getCloseModalWindowOK() {
        return closeModalWindowOK;
    }

    public void setCloseModalWindowOK(WebElement closeModalWindowOK) {
        this.closeModalWindowOK = closeModalWindowOK;
    }

    public WebElement getModalLetterIsSent() {
        return modalLetterIsSent;
    }

    public void setModalLetterIsSent(WebElement modalLetterIsSent) {
        this.modalLetterIsSent = modalLetterIsSent;
    }

    public WebElement getMakeLetter() {
        return makeLetter;
    }

    public void setMakeLetter(WebElement makeLetter) {
        this.makeLetter = makeLetter;
    }

    public WebElement getNumberOfInbox() {
        return numberOfInbox;
    }

    public void setNumberOfInbox(WebElement numberOfInbox) {
        this.numberOfInbox = numberOfInbox;
    }

    public WebElement getAllCheckbox() {
        return allCheckbox;
    }

    public void setAllCheckbox(WebElement allCheckbox) {
        this.allCheckbox = allCheckbox;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(WebElement deleteButton) {
        this.deleteButton = deleteButton;
    }
}
