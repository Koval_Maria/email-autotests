import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MakeLetterPage {
    MakeLetterPage() {
        PageFactory.initElements(MyWebDriver.getDriver(), this);
    }

    @FindBy(css = "[class='dline'] [name='send_to']")
    private WebElement sendToField;
    @FindBy(css = "[class='b_cnt'] [name='subject']")
    private WebElement subjectField;
    @FindBy(css = "[class='b_cnt'] [tabindex='8']")
    private WebElement inputLetterField;
    @FindBy(css = "[class='bottom_panel'] [tabindex='10']")
    private WebElement sendButton;
    @FindBy(css = "[class='dline_container'] [class='adiv'] [id='fromspan'])")
    private WebElement sender;

    public WebElement getSendToField() {
        return sendToField;
    }

    public void setSendToField(WebElement sendToField) {
        this.sendToField = sendToField;
    }

    public WebElement getSubjectField() {
        return subjectField;
    }

    public void setSubjectField(WebElement subjectField) {
        this.subjectField = subjectField;
    }

    public WebElement getInputLetterField() {
        return inputLetterField;
    }

    public void setInputLetterField(WebElement inputLetterField) {
        this.inputLetterField = inputLetterField;
    }

    public WebElement getSendButton() {
        return sendButton;
    }

    public void setSendButton(WebElement sendButton) {
        this.sendButton = sendButton;
    }

    public WebElement getSender() {
        return sender;
    }

    public void setSender(WebElement sender) {
        this.sender = sender;
    }
}

